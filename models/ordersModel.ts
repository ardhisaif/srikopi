import mongoose from "mongoose"
const {Schema} = mongoose

const ordersSchema = new Schema({
    billing: {},
    contact: {},
    items: [],
    payments: [],
    shipping: {},
    shippingStatus: '',
    subTotalPrice: Number,
    totalShipping: Number,
    totalPrice: Number,
    totalItems: Number,
    totalWeight: Number,
    user: {},
    createdAt: { type: Date, default: Date.now },
    payAt: { type: Date, default: Date.now }
})

const Orders = mongoose.model('Orders', ordersSchema)

module.exports = Orders