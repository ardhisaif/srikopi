import mongoose from "mongoose"
const {Schema} = mongoose

const userSchema = new Schema({
    name: String,
    contact: {
        phone : Number,
        email : String
    },
    password: "",
    createdAt: { type: Date, default: Date.now },
    updatedAt: { type: Date, default: Date.now }
})


const User = mongoose.model('User', userSchema)

module.exports = User