import { ObjectId } from "bson"
import mongoose from "mongoose"
const {Schema} = mongoose

const customerSchema = new Schema({
    name: String,
    userId: ObjectId,
    addresses: [
        {
            id: ObjectId,
            address: String,
            city: String,
            country: String,
            email: String,
            phone: Number,
            province: String
        }
    ],
    createdAt: { type: Date, default: Date.now },
    updatedAt: { type: Date, default: Date.now }
})


const Customer = mongoose.model('Customer', customerSchema)

module.exports = Customer