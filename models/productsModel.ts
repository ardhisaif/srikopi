import mongoose from "mongoose"
const {Schema} = mongoose

const productsSchema = new Schema({
    name: String,
    thumbnail: String,
    images: [],
    price: Number,
    categories: [],
    description: String,
    weight: Number,
    createdAt: { type: Date, default: Date.now },
    updatedAt: { type: Date, default: Date.now }
})


const Products = mongoose.model('Products', productsSchema)

module.exports = Products