import mongoose from "mongoose"
const {Schema} = mongoose

const productCategoriesSchema = new Schema({
    image: String,
    name: String,
    slug: String,
    priority: Number,
    createdAt: { type: Date, default: Date.now },
    updateAt: { type: Date, default: Date.now}
})


const ProductCategories = mongoose.model("productCategories", productCategoriesSchema, "productCategories")

module.exports = ProductCategories