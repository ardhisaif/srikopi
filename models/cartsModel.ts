import { ObjectId } from "bson"
import mongoose from "mongoose"
const {Schema} = mongoose

const cartSchema = new Schema({
    billing: {},
    contact: {},
    items: [],
    shipping: {},
    shippings: [],
    subTotalPrice: Number,
    taxes: [],
    totalItems: Number,
    totalPrice: Number,
    totalShipping: Number,
    totalWeight: Number,
    userId: ObjectId,
    createdAt: { type: Date, default: Date.now },
    updateAt: { type: Date, default: Date.now }
})

const Carts = mongoose.model('Cart', cartSchema)

module.exports = Carts