const {ObjectId} = require('mongodb');
const Products = require("../models/productsModel")
import {Request, Response} from "express"
import { Error } from "mongoose"

class ProductCategoriesController{
    
    static showProductCategories(req:Request, res:Response){
        Products.find({
            categories: ObjectId(req.params.id)
        })
            .then( (data:any) =>{
                res.send(data)
            })
            .catch((err:Error) => console.log(err))
    }

}

module.exports = ProductCategoriesController