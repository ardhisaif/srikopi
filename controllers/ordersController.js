
const mongoose = require("mongoose")
const Orders = require("../models/ordersModel")
const Carts = require("../models/cartsModel")
const Products = require("../models/productsModel")
const Customer = require("../models/customerModel")
const User = require("../models/userModel")
const { ObjectId } = require("bson")

class DetailOrdersController{

    static async createOrders(req, res){
        const session = await mongoose.startSession();
        const {userId, addressId} = req.body

        await session.withTransaction(async () => {
            let order = {
                billing: {},
                contact: {},
                items : [],
                payment: [],
                shipping: {},
                shippingStatus: 'PENDING',
                subTotalPrice: 0,
                totalShipping: 0,
                totalPrice: 0,
                totalItems: 0,
                totalWeight: 0,
                user: {}
            }

            const carts = await Carts.findOne({'userId':ObjectId(userId)})
            const customer = await Customer.findOne({userId: ObjectId(userId)})
            console.log(customer)
            for (let i = 0; i < customer.addresses.length; i++) {
                console.log(customer.addresses[i])
                if (customer.addresses[i].id === ObjectId(addressId) ) {

                    order.billing = customer.addresses[i]
                    order.shipping = customer.addresses[i]
                }
            }
            
            const user = await User.findById(userId)
            order.user = user
            for (let i = 0; i < carts.items.length; i++) {
                const productId = carts.items[i].productId;
                let item = {
                    cogs: 0,
                    description: '',
                    name: '',
                    price: 0,
                    quantity: 0,
                    weight: 0
                }
                
                const product = await Products.findById(productId)
                item.quantity = carts.items[i].quantity
                item.description = product.description
                item.name = product.name
                item.price = product.price
                item.cogs = item.price * item.quantity
                item.weight = product.weight
                order.totalWeight += item.weight * item.quantity
                order.totalItems += item.quantity
                order.subTotalPrice += item.cogs
                order.items.push(item)
            }

            order.contact = user.contact
            order.totalShipping = order.totalWeight * 3000
            order.totalPrice = order.subTotalPrice + order.totalShipping
            
            await Orders.create(order)
            const update = await Carts.updateOne(
                {"userId" : ObjectId(userId)},
                {$set: {items: [], totalItems: 0, totalPrice: 0, totalWeight: 0, totalShipping: 0, subTotalPrice: 0}})

            res.send(update)
        });

        session.endSession();
    }
}

module.exports = DetailOrdersController