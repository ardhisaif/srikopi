const Products = require("../models/productsModel")
import {Request, Response} from "express"
import { Error } from "mongoose"

class DetailProductController{

    static showProducts(req:Request, res:Response){
        Products.find()
            .then((data:any) => {
                res.send(data)
            })
            .catch((err:Error) => res.send(err))
    }
    
    static showDetailProduct(req:Request, res:Response){
        Products.findById(req.params.id)
            .then((data:any) => {
                res.send(data)
            })
            .catch((err:Error) => res.send(err))
    }

}

module.exports = DetailProductController