const {  ObjectId } = require("bson")
const mongoose = require("mongoose")
const Carts = require("../models/cartsModel")
const Product = require("../models/productsModel")
const {sum} = require("../helper/calculate")
import {Request,Response} from 'express'

class CartsController{
    
    static async calculateCart(req:Request, res:Response){
        
        const session = await mongoose.startSession();
        await session.withTransaction(async () => {           
            const {productId, userId} = req.body
            const quantity = Number(req.body.quantity)
            const id = `${productId}`
            const carts = await Carts.findOne({"userId" : ObjectId(userId), "items.productId" : ObjectId(id)})
            if (carts === null && quantity > 0) {
                await Carts.updateOne(
                        { $push: { items: [{productId : ObjectId(id), quantity}] } },
                    )
            }else{
                if (quantity < 1) {
                    await Carts.updateOne(
                        {"userId" : ObjectId(userId)},
                        {$pull:{items: {productId: ObjectId(id)}} }
                    )
                }else{
                    await Carts.updateOne(
                        {"userId" : ObjectId(userId), "items.productId" : ObjectId(id)},
                        {"$set" : {"items.$.quantity" : quantity}}
                    )
                }
            }
            const data = await Carts.findOne({"userId" : ObjectId(userId), "items.productId" : ObjectId(id)})
            let totalPrice = 0
            let totalWeight = 0
            for (let i = 0; i < data.items.length; i++) {
                const item = data.items[i];
                let product = await Product.findById(item.productId)
                totalPrice += product.price * item.quantity
                totalWeight += product.weight + item.quantity
            }

            let status = await Carts.updateOne(
                {"userId" : ObjectId(userId)},
                {$set: {totalItems: sum(data), totalPrice: totalPrice, totalWeight: totalWeight}})
            res.send(status)
        });
          
        session.endSession();

    }

    static showCart(req:Request, res:Response){
        
        const cartsId = "6155a0952229a618b10060ae"
        Carts.findById(ObjectId(cartsId))
            .then((data:any) => {
                res.send(data)
            })
            .catch((err:any) => console.log(err))
    }
}

module.exports = CartsController