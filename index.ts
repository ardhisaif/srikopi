import app from './server'
const port = process.env.PORT || 8080


app.listen(port,() =>{
    console.log(`server start on port ${port}`)
})