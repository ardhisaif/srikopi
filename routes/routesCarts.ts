import { Router } from "express"
const router = Router()  
const cartsController = require('../controllers/cartsController')

router.get('/', cartsController.showCart)
router.post('/', cartsController.calculateCart)

module.exports = router