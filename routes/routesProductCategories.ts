import { Router } from "express"
const router = Router()  
const DetailProductController = require('../controllers/productCategoriesController')

router.get('/:id', DetailProductController.showProductCategories)

module.exports = router