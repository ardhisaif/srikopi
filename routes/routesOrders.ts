import { Router } from "express"
const router = Router()  
const DetailOrdersController = require('../controllers/ordersController')

router.post('/', DetailOrdersController.createOrders)

module.exports = router