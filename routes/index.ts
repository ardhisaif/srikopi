
import { Router } from "express"
const router = Router()

const routesProduct = require("./routesProducts")
const routesProductCategories = require("./routesProductCategories")
const routesCarts = require("./routesCarts")
const routesOrders = require("./routesOrders")

router.use("/product", routesProduct)
router.use("/productCategories", routesProductCategories)
router.use("/carts", routesCarts)
router.use("/orders", routesOrders)

module.exports = router
