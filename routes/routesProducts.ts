import { Router } from "express"
const router = Router()  
const DetailProductController = require('../controllers/productControllers')

router.get('/', DetailProductController.showProducts)
router.get('/:id', DetailProductController.showDetailProduct)

module.exports = router